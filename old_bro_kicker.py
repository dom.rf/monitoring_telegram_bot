import os
import re
import keyring
import telebot
import psycopg2
from datetime import datetime
from ldap3 import Server, Connection, SAFE_SYNC, ALL
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

mon_bot_data = os.getenv("MON_BOT_DATA")

bot_poster_token = keyring.get_password("bot_poster_token", mon_bot_data)
bot_poster = telebot.TeleBot(str(bot_poster_token))

database_name = keyring.get_password("database_name", mon_bot_data)
db_user = keyring.get_password("db_user", mon_bot_data)
db_password = keyring.get_password("db_password", mon_bot_data)
db_test_host = keyring.get_password("db_test_host", mon_bot_data)
db_port = keyring.get_password("db_port", mon_bot_data)

db = psycopg2.connect(database=str(database_name), user=str(db_user), password=str(db_password), host=str(db_test_host), port=str(db_port))
cursor = db.cursor()

date_now = datetime.now()
date_now = date_now.strftime("%d-%m-%Y %H:%M")

def find_and_kick():
    cursor.execute("SELECT login FROM public.users")
    users = cursor.fetchall()
    for i in users:
        if i[0] is not None:
            try:
                roscap_mon_user = keyring.get_password("roscap_mon_user", mon_bot_data)
                roscap_mon_pass = keyring.get_password("roscap_mon_pass", mon_bot_data)
                server = Server("dc.roscap.com", get_info=ALL)
                conn = Connection(server, str(roscap_mon_user), str(roscap_mon_pass), client_strategy=SAFE_SYNC, auto_bind=True)
                status, result, response, _ = conn.search('DC=roscap,DC=com', '''(sAMAccountName=''' + str(i[0]) + ''')''', attributes=['mobile'])
                if re.findall(r"Уволенные", str(response[0]["dn"])):
                    cursor.execute("SELECT chat_name FROM public.users_tmp_invite_links WHERE login = \'"+str(i[0])+"\'")
                    chat_names = cursor.fetchall()
                    cursor.execute("SELECT tg_user_id FROM public.users WHERE login = \'" + str(i[0]) + "\'")
                    users_tg_id = cursor.fetchone()
                    for x in chat_names:
                        try:
                            if re.findall(r"^Zabbix.*", str(x[0])):
                                cursor.execute("SELECT chat_id FROM public.zabbix_telegram_groups WHERE chat_name = \'"+str(x[0])+"\'")
                                zabbix_chatid = cursor.fetchone()
                                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(x[0]) + "\' AND login = \'"+str(i[0])+"\'")
                                invite_link = cursor.fetchone()
                                if invite_link is not None:
                                    bot_poster.revoke_chat_invite_link(chat_id="-" + str(zabbix_chatid[0]), invite_link=str(invite_link[0]))
                                bot_poster.ban_chat_member(chat_id="-" + str(zabbix_chatid[0]), user_id=int(users_tg_id[0]))
                                cursor.execute("DELETE FROM public.users_tmp_invite_links WHERE login = \'" + str(i[0]) + "\' AND chat_name = \'"+str(x[0])+"\'")
                                db.commit()
                            elif re.findall(r"^Victoria.*", str(x[0])):
                                org_name = re.findall(r"(?<=^VictoriaMetrics Дом.рф ).*", str(x[0]))
                                cursor.execute("SELECT chat_id FROM public.vm_telegram_groups WHERE org_name = \'"+str(org_name[0])+"\'")
                                victoriametrics_chatid = cursor.fetchone()
                                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(x[0]) + "\' AND login = \'"+str(i[0])+"\'")
                                invite_link = cursor.fetchone()
                                if invite_link is not None:
                                    bot_poster.revoke_chat_invite_link(chat_id="-" + str(victoriametrics_chatid[0]), invite_link=str(invite_link[0]))
                                bot_poster.ban_chat_member(chat_id="-" + str(victoriametrics_chatid[0]), user_id=int(users_tg_id[0]))
                                cursor.execute("DELETE FROM public.users_tmp_invite_links WHERE login = \'" + str(i[0]) + "\' AND chat_name = \'"+str(x[0])+"\'")
                                db.commit()
                        except telebot.apihelper.ApiException as e:
                            os.popen("echo "+str(date_now)+ " ##### ERROR: "+str(e)+", User:" + str(i[0]) + ", Chatname: "+ str(x[0])+ " >> /tmp/old_kicker.log")

            except KeyError:
                ahml1_mon_user = keyring.get_password("ahml1_mon_user", mon_bot_data)
                ahml1_mon_pass = keyring.get_password("ahml1_mon_pass", mon_bot_data)
                server = Server("dc.ahml1.ru", get_info=ALL)
                conn = Connection(server, str(ahml1_mon_user), str(ahml1_mon_pass), client_strategy=SAFE_SYNC, auto_bind=True)
                status, result, response, _ = conn.search('DC=ahml1,DC=ru', '''(sAMAccountName=''' + str(i[0]) + ''')''', attributes=['mobile'])
                if re.findall(r"Уволенные", str(response[0]["dn"])):
                    cursor.execute("SELECT chat_name FROM public.users_tmp_invite_links WHERE login = \'" + str(i[0]) + "\'")
                    chat_names = cursor.fetchall()
                    cursor.execute("SELECT tg_user_id FROM public.users WHERE login = \'" + str(i[0]) + "\'")
                    users_tg_id = cursor.fetchone()
                    for x in chat_names:
                        try:
                            if re.findall(r"^Zabbix.*", str(x[0])):
                                cursor.execute("SELECT chat_id FROM public.zabbix_telegram_groups WHERE chat_name = \'" + str(x[0]) + "\'")
                                zabbix_chatid = cursor.fetchone()
                                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(x[0]) + "\' AND login = \'"+str(i[0])+"\'")
                                invite_link = cursor.fetchone()
                                if invite_link is not None:
                                    bot_poster.revoke_chat_invite_link(chat_id="-" + str(zabbix_chatid[0]), invite_link=str(invite_link[0]))
                                bot_poster.ban_chat_member(chat_id="-" + str(zabbix_chatid[0]), user_id=int(users_tg_id[0]))
                                cursor.execute("DELETE FROM public.users_tmp_invite_links WHERE login = \'" + str(i[0]) + "\' AND chat_name = \'"+str(x[0])+"\'")
                                db.commit()
                            elif re.findall(r"^Victoria.*", str(x[0])):
                                org_name = re.findall(r"(?<=^VictoriaMetrics Дом.рф ).*", str(x[0]))
                                cursor.execute("SELECT chat_id FROM public.vm_telegram_groups WHERE org_name = \'" + str(org_name) + "\'")
                                victoriametrics_chatid = cursor.fetchone()
                                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(x[0]) + "\' AND login = \'"+str(i[0])+"\'")
                                invite_link = cursor.fetchone()
                                if invite_link is not None:
                                    bot_poster.revoke_chat_invite_link(chat_id="-" + str(victoriametrics_chatid[0]), invite_link=str(invite_link[0]))
                                bot_poster.ban_chat_member(chat_id="-" + str(victoriametrics_chatid[0]), user_id=int(users_tg_id[0]))
                                cursor.execute("DELETE FROM public.users_tmp_invite_links WHERE login = \'" + str(i[0]) + "\' AND chat_name = \'"+str(x[0])+"\'")
                                db.commit()
                        except telebot.apihelper.ApiException as e:
                            os.popen("echo "+str(date_now)+ " ##### ERROR: "+str(e)+", User:" + str(i[0]) + ", Chatname: "+ str(x[0])+ " >> /tmp/old_kicker.log")


find_and_kick()
