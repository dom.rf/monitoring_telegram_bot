import re
import os
import json
import time
import random
import keyring
import requests
from pyzabbix import ZabbixAPI
import psycopg2
import subprocess
import telebot
from telebot.util import async_dec
from ldap3 import Server, Connection, SAFE_SYNC, ALL
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

mon_bot_data = os.getenv("MON_BOT_DATA")

bot_token = keyring.get_password("bot_token", mon_bot_data)
bot_poster_token = keyring.get_password("bot_poster_token", mon_bot_data)

bot = telebot.TeleBot(str(bot_token))
bot_poster = telebot.TeleBot(str(bot_poster_token))

char_number_set = {"0": "A", "1": "B", "2": "C", "3": "D", "4": "E", "5": "F",
                   "6": "G", "7": "H", "8": "I", "9": "J", "10": "K", "11": "L",
                   "12": "M", "13": "N", "14": "O", "15": "P", "16": "Q", "17": "R",
                   "18": "S", "19": "T", "20": "U", "21": "V", "22": "W", "23": "X",
                   "24": "Y", "25": "Z"}

database_name = keyring.get_password("database_name", mon_bot_data)
db_user = keyring.get_password("db_user", mon_bot_data)
db_password = keyring.get_password("db_password", mon_bot_data)
db_host = keyring.get_password("db_host", mon_bot_data)
db_port = keyring.get_password("db_port", mon_bot_data)

db = psycopg2.connect(database=str(database_name), user=str(db_user), password=str(db_password), host=str(db_host), port=str(db_port))
cursor = db.cursor()

cursor.execute("""CREATE TABLE IF NOT EXISTS public.users (
                                    login TEXT,
                                    nickname TEXT,
                                    tg_user_id BIGINT,
                                    last_sms_code INTEGER,
                                    last_random_choice TEXT,
                                    user_phone TEXT,
                                    last_user_answer TEXT,
                                    cycle_breaker INTEGER)""")
db.commit()
cursor.execute("""CREATE TABLE IF NOT EXISTS public.zabbix_telegram_groups (
                                    chat_name TEXT,
                                    chat_id BIGINT,
                                    invite_link TEXT,
                                    monitoring_user TEXT)""")
db.commit()
cursor.execute("""CREATE TABLE IF NOT EXISTS public.users_tmp_invite_links (
                                    tg_user_id BIGINT,
                                    login TEXT,
                                    chat_name TEXT,
                                    tmp_invite_link TEXT)""")
db.commit()
cursor.execute("""CREATE TABLE IF NOT EXISTS public.vm_telegram_groups (
                                    org_name TEXT,
                                    chat_id BIGINT,
                                    invite_link TEXT,
                                    ad_group_name TEXT)""")
db.commit()

zabbix_api_user = keyring.get_password("zabbix_api_user", mon_bot_data)
zabbix_api_pass = keyring.get_password("zabbix_api_pass", mon_bot_data)
grafana_api_user = keyring.get_password("grafana_api_user", mon_bot_data)
grafana_api_pass = keyring.get_password("grafana_api_pass", mon_bot_data)


@bot.message_handler(commands=["start"])
def welcome_game_start(message):
    rand_choice = random.choice(["Камень🤜", "Ножницы✌", "Бумага✋"])
    cursor.execute("SELECT tg_user_id FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    if cursor.fetchone() is None:
        cursor.execute("INSERT INTO public.users (tg_user_id) VALUES ('"+str(message.from_user.id)+"')")
        db.commit()
        cursor.execute("UPDATE public.users SET last_random_choice = \'" + str(rand_choice) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
        db.commit()
    else:
        cursor.execute("UPDATE public.users SET last_random_choice = \'"+str(rand_choice)+"\' WHERE tg_user_id = \'"+str(message.from_user.id)+"\'")
        db.commit()
    welcome_to_game = bot.send_message(message.chat.id, "Привет! Давайте сыграем в игру. Сможете меня победить?\n\n" + str(rand_choice))
    rmk = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True).add(telebot.types.KeyboardButton("Камень🤜"), telebot.types.KeyboardButton("Ножницы✌"), telebot.types.KeyboardButton("Бумага✋"))
    bot.send_message(message.chat.id, "Ваш ход!", reply_markup=rmk)
    bot.register_next_step_handler(welcome_to_game, check_answer_and_login)
    cursor.execute("UPDATE public.users SET cycle_breaker = \'" + str("0") + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    db.commit()


def check_answer_and_login(message):
    user_answer = message.text
    cursor.execute("UPDATE public.users SET last_user_answer = \'"+str(user_answer)+"\' WHERE tg_user_id = \'"+str(message.from_user.id)+"\'")
    db.commit()
    cursor.execute("SELECT last_user_answer FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    last_user_answer = cursor.fetchone()
    cursor.execute("SELECT last_random_choice FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    last_random_choice = cursor.fetchone()
    if str(last_random_choice[0]) == str("Камень🤜") and str(last_user_answer[0]) == str("Бумага✋"):
        check_result = bot.send_message(message.chat.id, "Ок, кажется Вы не робот!\n\nВведите Ваш логин:")
        bot.register_next_step_handler(check_result, choice_domain)
    elif str(last_random_choice[0]) == str("Ножницы✌") and str(last_user_answer[0]) == str("Камень🤜"):
        check_result = bot.send_message(message.chat.id, "Ок, кажется Вы не робот!\n\nВведите Ваш логин:")
        bot.register_next_step_handler(check_result, choice_domain)
    elif str(last_random_choice[0]) == str("Бумага✋") and str(last_user_answer[0]) == str("Ножницы✌"):
        check_result = bot.send_message(message.chat.id, "Ок, кажется Вы не робот!\n\nВведите Ваш логин:")
        bot.register_next_step_handler(check_result, choice_domain)
    else:
        bot.send_message(message.chat.id, "Кажется у Вас есть пара шестеренок!")


def choice_domain(message):
    user_login = message.text
    cursor.execute("SELECT login FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    user_login_in_db = cursor.fetchone()
    if cursor.fetchone() is None or user_login_in_db[0] != user_login:
        cursor.execute("UPDATE public.users SET login = \'"+str(user_login)+"\' WHERE tg_user_id = \'"+str(message.from_user.id)+"\'")
        db.commit()
    rmk = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    rmk.add(telebot.types.KeyboardButton("Банк ДОМ.РФ"), telebot.types.KeyboardButton("ДОМ.РФ"))
    choice = bot.send_message(message.chat.id, "Выберите организацию: ", reply_markup=rmk)
    bot.register_next_step_handler(choice, check_user_is_fired)


def check_user_is_fired(message):
    domain = message.text
    cursor.execute("SELECT login FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    user_login = cursor.fetchone()
    if domain == str("Банк ДОМ.РФ"):
        roscap_mon_user = keyring.get_password("roscap_mon_user", mon_bot_data)
        roscap_mon_pass = keyring.get_password("roscap_mon_pass", mon_bot_data)
        server = Server("dc.roscap.com", get_info=ALL)
        conn = Connection(server, str(roscap_mon_user), str(roscap_mon_pass), client_strategy=SAFE_SYNC, auto_bind=True)
        status, result, response, _ = conn.search('DC=roscap,DC=com', '''(sAMAccountName=''' + str(user_login[0]) + ''')''', attributes=['mobile'])
        try:
            if re.findall(r"Уволенные", str(response[0]["dn"])):
                bot.send_message(message.chat.id, "Извините, но Ваша УЗ неактивна. В доступе отказано.")
            else:
                try:
                    phone_number = re.findall(r"\d+", str(response[0]["raw_attributes"]["mobile"][0]))
                    if re.findall(r"^8", str(phone_number[0])):
                        phone_number = re.sub(r"^8", "+7", str(phone_number[0]))
                        phone_number_masked = re.sub(r"....(?=...$)", "****", str(phone_number))
                        msg = bot.send_message(message.chat.id, "На Ваш номер " + str(phone_number_masked) + " отправлено сообщение.\nПройдите SMS-верификацию:\n")
                        sms_code = ""
                        lst = [random.randint(0, 9) for i in range(6)]
                        for i in lst:
                            sms_code += str(i)
#                        subprocess.call(["/usr/lib/zabbix/alertscripts/smstraffic.sh", str(phone_number), "Verification code: "+str(sms_code)])
                        cursor.execute("UPDATE public.users SET last_sms_code = \'" + str(sms_code) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        cursor.execute("UPDATE public.users SET user_phone = \'" + str(phone_number) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        bot.register_next_step_handler(msg, sms_check)
                    else:
                        phone_number = ("+" + str(phone_number[0]))
                        phone_number_masked = re.sub(r"....(?=...$)", "****", str(phone_number))
                        msg = bot.send_message(message.chat.id, "На Ваш номер " + str(phone_number_masked) + " отправлено сообщение.\nПройдите SMS-верификацию:\n")
                        sms_code = ""
                        lst = [random.randint(0, 9) for i in range(6)]
                        for i in lst:
                            sms_code += str(i)
#                        subprocess.call(["/usr/lib/zabbix/alertscripts/smstraffic.sh", str(phone_number), "Verification code: " + str(sms_code)])
                        cursor.execute("UPDATE public.users SET last_sms_code = \'" + str(sms_code) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        cursor.execute("UPDATE public.users SET user_phone = \'" + str(phone_number) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        bot.register_next_step_handler(msg, sms_check)
                except IndexError:
                    bot.send_message(message.chat.id, "У Вашей УЗ нет привязанного телефона в Active Directory.\nОбратитесь к администраторам AD для решения проблемы.")
        except KeyError:
            bot.send_message(message.chat.id, "Извините, но пользователя с такой УЗ не обнаружено. Проверьте вводимые данные.\n\n/start")

    elif domain == str("ДОМ.РФ"):
        ahml1_mon_user = keyring.get_password("ahml1_mon_user", mon_bot_data)
        ahml1_mon_pass = keyring.get_password("ahml1_mon_pass", mon_bot_data)
        server = Server("dc.ahml1.ru", get_info=ALL)
        conn = Connection(server, str(ahml1_mon_user), str(ahml1_mon_pass), client_strategy=SAFE_SYNC, auto_bind=True)
        status, result, response, _ = conn.search('DC=ahml1,DC=ru', '''(sAMAccountName=''' + str(user_login[0]) + ''')''', attributes=['mobile'])
        try:
            if re.findall(r"Уволенные", str(response[0]["dn"])):
                bot.send_message(message.chat.id, "Извините, но Ваша УЗ неактивна. В доступе отказано.")
            else:
                try:
                    phone_number = re.findall(r"\d+", str(response[0]["raw_attributes"]["mobile"][0]))
                    if re.findall(r"^8", str(phone_number[0])):
                        phone_number = re.sub(r"^8", "+7", str(phone_number[0]))
                        phone_number_masked = re.sub(r"....(?=...$)", "****", str(phone_number))
                        msg = bot.send_message(message.chat.id, "На Ваш номер " + str(phone_number_masked) + " отправлено сообщение.\nПройдите SMS-верификацию:\n")
                        sms_code = ""
                        lst = [random.randint(0, 9) for i in range(6)]
                        for i in lst:
                            sms_code += str(i)
#                        subprocess.call(["/usr/lib/zabbix/alertscripts/smstraffic.sh", str(phone_number), "Verification code: " + str(sms_code)])
                        cursor.execute("UPDATE public.users SET last_sms_code = \'" + str(sms_code) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        cursor.execute("UPDATE public.users SET user_phone = \'" + str(phone_number) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        bot.register_next_step_handler(msg, sms_check)
                    else:
                        phone_number = ("+" + str(phone_number[0]))
                        phone_number_masked = re.sub(r"....(?=...$)", "****", str(phone_number))
                        msg = bot.send_message(message.chat.id, "На Ваш номер " + str(phone_number_masked) + " отправлено сообщение.\nПройдите SMS-верификацию:\n")
                        sms_code = ""
                        lst = [random.randint(0, 9) for i in range(6)]
                        for i in lst:
                            sms_code += str(i)
#                        subprocess.call(["/usr/lib/zabbix/alertscripts/smstraffic.sh", str(phone_number), "Verification code: " + str(sms_code)])
                        cursor.execute("UPDATE public.users SET last_sms_code = \'" + str(sms_code) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        cursor.execute("UPDATE public.users SET user_phone = \'" + str(phone_number) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
                        db.commit()
                        bot.register_next_step_handler(msg, sms_check)
                except IndexError:
                    bot.send_message(message.chat.id, "У пользователя нет привязанного телефона в Active Directory.\nОбратитесь к администраторам AD для обогащения данных пользователя.")
        except KeyError:
            bot.send_message(message.chat.id, "Извините, но пользователя с такой УЗ не обнаружено. Проверьте вводимые данные.\n\n/start")

    else:
        bot.send_message(message.chat.id, "Такого домена не сущетсвует, попробуйте еще раз.\n\n/start")


def sms_check(message):
    code = message.text
    cursor.execute("SELECT last_sms_code FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    last_sms_code = cursor.fetchone()
    try:
        if int(last_sms_code[0]) == int(code):
            bot.send_message(message.chat.id, "Ок, код совпадает")
            cursor.execute("UPDATE public.users SET nickname = \'" + str(message.from_user.username) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
            db.commit()
            choice_alert_system(message)
        else:
            bot.send_message(message.chat.id, "Код не совпадает")
    except ValueError:
        bot.send_message(message.chat.id, "Я не понимаю что Вы вводите! 🤷\nНеобходимо ввести только цифры кода из SMS\nПопробуйте еще раз.\n\n/start")


def choice_alert_system(message):
    rmk = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    rmk.add(telebot.types.KeyboardButton("Zabbix"), telebot.types.KeyboardButton("VictoriaMetrics"))
    choice = bot.send_message(message.chat.id, "Выберите систему мониторинга: ", reply_markup=rmk)
    bot.register_next_step_handler(choice, check_user_monitoring_system)


def check_user_monitoring_system(message):
    alert_system = message.text
    if alert_system == "Zabbix":
        zabbix_show_alert_groups(message)
    elif alert_system == "VictoriaMetrics":
        victoria_show_alert_groups(message)
    else:
        bot.send_message(message.chat.id, "Такой системы мониторинга нет. Попробуйте еще раз.\n\n/start")


@async_dec()
def zabbix_show_alert_groups(message):
    cursor.execute("SELECT login FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    user_login = cursor.fetchone()
    user_zabbix_telegram_groups = ""
    count_if_no_access_zabbix = 0
    bot.send_message(message.chat.id, "Ищем доступные группы...")
    zapi = ZabbixAPI("https://zabbix-api.domrfbank.ru/api_jsonrpc.php")
    zapi.session.verify = False
    zapi.login(zabbix_api_user, zabbix_api_pass)
    user = user_login[0]
    get_users = requests.get(f"http://l-zbx-self-bnk.roscap.com:3000/api/users?query={str(user)}", auth=(str(grafana_api_user), str(grafana_api_pass)), verify=False).json()
    if get_users is None:
        bot.send_message(message.chat.id, "Пользователя с УЗ " + str(user_login[0]) + " в Grafana не обнаружено. Проверьте правильность ввода данных. Помните, что регистр важен!\n\n/start")
    else:
        get_users_orgs = []
        get_all_orgs = requests.get(f"http://l-zbx-self-bnk.roscap.com:3000/api/admin/ldap/{user}", auth=("linux-monsrv-user", "xD(37X1H2&g("), verify=False).json()
        for org_id in get_all_orgs["roles"]:
            if org_id["orgId"] != 0:
                get_users_orgs.append({"orgId": org_id["orgId"]})

        for i in get_users_orgs:
            if str(i["orgId"]) != str("1") and str(i["orgId"]) != str("0") and str(i["orgId"]) != str("74"):
                requests.post("http://l-zbx-self-bnk.roscap.com:3000/api/user/using/" + str(i["orgId"]), auth=(str(grafana_api_user), str(grafana_api_pass)), verify=False).json()
                get_all_ds = requests.get("http://l-zbx-self-bnk.roscap.com:3000/api/datasources", auth=(str(grafana_api_user), str(grafana_api_pass)), verify=False).json()
                for x in get_all_ds:
                    if x["type"] == "alexanderzobnin-zabbix-datasource" and str(x["jsonData"]["username"]) != "zbx_grafana_all_hostgroups" and str(x["url"]) == "https://zabbix-api.domrfbank.ru/api_jsonrpc.php" and re.findall(r"^Zabbix.*?$", str(x["name"])):
                        count_if_no_access_zabbix += 1
                        create_tmp_link = ""
                        cursor.execute("SELECT monitoring_user FROM public.zabbix_telegram_groups WHERE monitoring_user = \'" + str(x["jsonData"]["username"]) + "\'")
                        if cursor.fetchone() is None:
                            created_invite_link = ""
                            while len(created_invite_link) < 2:
                                try:
                                    tg_group_name = re.sub(r"Zabbix-", "Zabbix Дом.рф ", str(x["name"]))
                                    tg_group_name = re.sub(r",", ".", tg_group_name)
                                    run_creator = os.popen("""/usr/bin/zabbix_get -s l-zpx-ext-prv-bnk.roscap.com -k \"system.run[sudo /usr/bin/python3 /usr/lib/zabbix/externalscripts/group_creator.py '""" + str(tg_group_name) + """']\" """).read()
                                    run_creator = re.sub("'", '"', str(run_creator))
                                    run_creator = json.loads(run_creator)
                                    created_chat_id = run_creator[0]["chat_id"]
                                    created_invite_link = run_creator[0]["invite_link"]
                                    tg_group_name = run_creator[0]["tg_group_name"]
                                    cursor.execute("INSERT INTO public.zabbix_telegram_groups (chat_name, chat_id, invite_link, monitoring_user) VALUES ('" + str(tg_group_name) + "', '" + str(created_chat_id) + "', '" + str(created_invite_link) + "', '" + str(x["jsonData"]["username"]) + "')")
                                    db.commit()
                                    create_tmp_link = bot_poster.create_chat_invite_link(chat_id="-" + str(created_chat_id), member_limit=1)
                                    create_tmp_link = create_tmp_link.to_json()
                                    create_tmp_link = json.loads(create_tmp_link)
                                    cursor.execute("INSERT INTO public.users_tmp_invite_links (tg_user_id, login, chat_name, tmp_invite_link) VALUES ('" + str(message.from_user.id) + "', '" + str(user) + "', '" + str(tg_group_name) + "', '" + str(create_tmp_link["invite_link"]) + "')")
                                    db.commit()
                                    get_user_media = zapi.user.get(filter={"username": str(x["jsonData"]["username"])}, selectMedias=["severity", "sendto", "mediatypeid"], output=["medias"], selectUsrgrps=True)
                                    zabbix_userid = get_user_media[0]["userid"]
                                    if len(str(get_user_media[0]["medias"])) > 2:
                                        found_tg_notofocation = 0
                                        for m in get_user_media[0]["medias"]:
                                            if m["mediatypeid"] == "25" and m["sendto"] == "-" + str(created_chat_id):
                                                found_tg_notofocation += 1
                                        if found_tg_notofocation == 0:
                                            json_data = json.dumps(get_user_media[0]["medias"])
                                            json_data = json.loads(json_data)
                                            json_data.append({"severity": "63", "sendto": "-" + str(created_chat_id), "mediatypeid": "25"})
                                            zapi.user.update(userid=str(zabbix_userid), medias=json_data)
                                    else:
                                        zapi.user.update(userid=str(zabbix_userid), medias=[{"severity": "63", "sendto": "-" + str(created_chat_id), "mediatypeid": "25"}])
                                    get_usergroupid = zapi.usergroup.get(userids=str(zabbix_userid))
                                    letter_count = 0
                                    action_formula = ""
                                    conditions_json = ""
                                    for ug in get_usergroupid:
                                        if int(ug["usrgrpid"]) != 13:
                                            get_rights = zapi.usergroup.get(usrgrpids=str(ug["usrgrpid"]), selectRights=["id", "permission"])
                                            for ur in get_rights[0]["rights"]:
                                                if int(ur["permission"]) != 0:
                                                    action_formula = (action_formula + str(char_number_set[str(letter_count)]) + " ")
                                                    conditions_json = (conditions_json + str("""{"conditiontype":"0", "operator":"0", "value":\"""" + ur["id"] + """\", "formulaid":\"""" + str(char_number_set[str(letter_count)]) + """\"},"""))
                                                    letter_count += 1
                                    action_formula = re.sub(r"\s(?!$)", " or ", action_formula)
                                    action_formula = re.sub(r"\s(?=$)", "", action_formula)
                                    conditions_json = (conditions_json + """{"conditiontype":"4", "operator":"5", "value":"1", "formulaid": \"""" + str(char_number_set[str(letter_count)]) + """\"}""")
                                    last_condition = str(char_number_set[str(letter_count)])
                                    conditions_json = re.sub(r",$", "", conditions_json)
                                    filter_json = """{"evaltype": 3, "formula": "(""" + str(action_formula) + """) and """ + str(last_condition) + """\", "conditions": [""" + str(conditions_json) + """]}"""
                                    filter_json = json.loads(filter_json)
                                    find_action_by_name = zapi.action.get(filter={"name": str(x["jsonData"]["username"]) + "-telegram-notification"})
                                    if len(str(find_action_by_name)) > 2:
                                        None
                                    else:
                                        zapi.action.create(name=str(x["jsonData"]["username"]) + "-telegram-notification", eventsource=0, status=0, esc_period="1h", pause_suppressed=1, filter=filter_json,
                                                           operations=[{"operationtype": "0", "esc_period": "0", "esc_step_from": "1", "esc_step_to": "1", "evaltype": "0", "opconditions": [], "opmessage": {"default_msg": "0", "subject": "{TRIGGER.STATUS} : {TRIGGER.NAME}", "message": "Триггер: {TRIGGER.NAME}\r\nУровень: {TRIGGER.SEVERITY}\r\nДата события: {EVENT.DATE}\r\nВремя события: {EVENT.TIME}", "mediatypeid": "25"}, "opmessage_grp": [], "opmessage_usr": [{"userid": str(zabbix_userid)}]}],
                                                           update_operations=[{"operationtype": "0", "opmessage": {"default_msg": "0", "subject": "Обновление статуса : {TRIGGER.NAME}", "message": "{USER.FULLNAME} обновил статус проблемы {ACK.DATE} в {ACK.TIME}, со следующим сообщением:\r\n{ACK.MESSAGE}\r\n\r\nТекущий статус проблемы {EVENT.STATUS}", "mediatypeid": "25"}, "opmessage_grp": [], "opmessage_usr": [{"userid": str(zabbix_userid)}]}],
                                                           recovery_operations=[{"operationtype": "0", "opmessage": {"default_msg": "0", "subject": "{TRIGGER.STATUS} : {TRIGGER.NAME}", "message": "Триггер: {TRIGGER.NAME}\r\nУровень: {TRIGGER.SEVERITY}\r\nДата события: {EVENT.DATE}\r\nВремя события: {EVENT.TIME}", "mediatypeid": "25"}, "opmessage_grp": [], "opmessage_usr": [{"userid": str(zabbix_userid)}]}])
                                    time.sleep(2)
                                except Exception as e:
                                    print(e)
                            user_zabbix_telegram_groups = (user_zabbix_telegram_groups + "Чат оповещения из Zabbix: " + str(tg_group_name) + "\nПригласительная ссылка: " + str(create_tmp_link["invite_link"]) + "\n")
                        else:
                            cursor.execute("SELECT chat_name FROM public.zabbix_telegram_groups WHERE monitoring_user = \'" + str(x["jsonData"]["username"]) + "\'")
                            tg_group_name = cursor.fetchone()
                            cursor.execute("SELECT chat_id FROM public.zabbix_telegram_groups WHERE monitoring_user = \'" + str(x["jsonData"]["username"]) + "\'")
                            created_chat_id = cursor.fetchone()
                            cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(tg_group_name[0]) + "\' AND login = \'" + str(user) + "\'")
                            created_invite_tmp_link = cursor.fetchone()
                            time.sleep(2)
                            if created_invite_tmp_link is None:
                                create_tmp_link = bot_poster.create_chat_invite_link(chat_id="-" + str(created_chat_id[0]), member_limit=1)
                                create_tmp_link = create_tmp_link.to_json()
                                create_tmp_link = json.loads(create_tmp_link)
                                cursor.execute("INSERT INTO public.users_tmp_invite_links (tg_user_id, login, chat_name, tmp_invite_link) VALUES ('" + str(message.from_user.id) + "', '" + str(user) + "', '" + str(tg_group_name[0]) + "', '" + str(create_tmp_link["invite_link"]) + "')")
                                db.commit()
                                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE tg_user_id = \'" + str(message.from_user.id) + "\' AND login = \'" + str(user) + "\' AND chat_name = \'" + str(tg_group_name[0]) + "\'")
                                created_invite_tmp_link = cursor.fetchone()

                            time.sleep(5)
                            if len(user_zabbix_telegram_groups) < 2000:
                                user_zabbix_telegram_groups = (user_zabbix_telegram_groups + "Чат оповещения из Zabbix: " + str(tg_group_name[0]) + "\nПригласительная ссылка: " + str(created_invite_tmp_link[0]) + "\n---------------------------------------------------------------------------\n")
                            else:
                                bot.send_message(message.chat.id, str(user_zabbix_telegram_groups))
                                user_zabbix_telegram_groups = ""
                                user_zabbix_telegram_groups = (user_zabbix_telegram_groups + "Чат оповещения из Zabbix: " + str(tg_group_name[0]) + "\nПригласительная ссылка: " + str(created_invite_tmp_link[0]) + "\n---------------------------------------------------------------------------\n")

                if count_if_no_access_zabbix == 0:
                    bot.send_message(message.chat.id, "Группы для Вас не обнаружены.\nДля получения доступа Вам необходимо назначить группу графаны Вашей организации/АС")

        if len(user_zabbix_telegram_groups) > 0:
            bot.send_message(message.chat.id, str(user_zabbix_telegram_groups))
            user_zabbix_telegram_groups = ""
        bot.send_message(message.chat.id, "Поиск групп Zabbix завершен.")
        cursor.execute("SELECT cycle_breaker FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
        cycle_breaker = cursor.fetchone()
        cycle_breaker = (int(cycle_breaker[0]) + 1)
        cursor.execute("UPDATE public.users SET cycle_breaker = \'" + str(cycle_breaker) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
        db.commit()
        cursor.execute("SELECT cycle_breaker FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
        cycle_breaker = cursor.fetchone()
        time.sleep(5)
        if int(cycle_breaker[0]) < 2:
            goto_victoria(message)
        else:
            bot.send_message(message.chat.id, "Поиск завершен. Спасибо за использование бота мониторинга!")

        zapi.user.logout()


def goto_victoria(message):
    rmk = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    rmk.add(telebot.types.KeyboardButton("Да"), telebot.types.KeyboardButton("Нет"))
    choice = bot.send_message(message.chat.id, "Хотите продолжить и поискать группы VictoriaMetrics?\n"
                                               "❗ Воспользуйтесь этим пунктом ТОЛЬКО в случае, когда действительно есть необходимость в мониторинге средствами VictoriaMetrics", reply_markup=rmk)
    bot.register_next_step_handler(choice, go_next_victoria)


def go_next_victoria(message):
    alert_system = message.text
    if alert_system == "Да":
        victoria_show_alert_groups(message)
    elif alert_system == "Нет":
        bot.send_message(message.chat.id, "Ок, спасибо за использование бота мониторинга!")
    else:
        bot.send_message(message.chat.id, "Извините, но я Вас не понял. Попробуйте начать сначала!\n\n/start")


@async_dec()
def victoria_show_alert_groups(message):
    cursor.execute("SELECT login FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    user_login = cursor.fetchone()
    user_victoria_telegram_groups = ""
    user = user_login[0]
    count_if_no_access_victoria = 0
    bot.send_message(message.chat.id, "Ищем доступные группы...")
    get_user_groups = requests.get("http://l-zbx-self-bnk.roscap.com:3000/api/admin/ldap/" + str(user), auth=(str(grafana_api_user), str(grafana_api_pass)), verify=False).json()
    for i in get_user_groups["roles"]:
        if i["orgId"] != 0 and i["orgId"] != 1 and i["orgId"] != 74:
            ad_group_body = re.findall(r"gsl-grafana.*?(?=.rw|.ro)", i["groupDN"])
            victoria_tg_group_name = ("VictoriaMetrics Дом.рф " + re.sub(r",", ".", str(i["orgName"])))
            cursor.execute("SELECT org_name FROM public.vm_telegram_groups WHERE org_name = \'" + re.sub(r",", ".", str(i["orgName"])) + "\'")
            count_if_no_access_victoria += 1
            if cursor.fetchone() is None and not re.findall(r".*gsl-grafana.ro.*", str(i["groupDN"])):
                created_invite_link = ""
                while len(created_invite_link) < 2:
                    try:
                        run_creator = os.popen("""/usr/bin/zabbix_get -s l-zpx-ext-prv-bnk.roscap.com -k \"system.run[sudo /usr/bin/python3 /usr/lib/zabbix/externalscripts/group_creator.py '""" + str(victoria_tg_group_name) + """']\" """).read()
                        run_creator = re.sub("'", '"', str(run_creator))
                        run_creator = json.loads(run_creator)
                        created_chat_id = run_creator[0]["chat_id"]
                        created_invite_link = run_creator[0]["invite_link"]
                        victoria_tg_group_name = run_creator[0]["tg_group_name"]
                        cursor.execute("INSERT INTO public.vm_telegram_groups (org_name, chat_id, invite_link, ad_group_name) VALUES ('" + re.sub(r",", ".", str(i["orgName"])) + "', '" + str(created_chat_id) + "', '" + str(created_invite_link) + "', '" + str(ad_group_body[0]) + "')")
                        db.commit()
                        victoria_create_tmp_link = bot_poster.create_chat_invite_link(chat_id="-" + str(created_chat_id), member_limit=1)
                        victoria_create_tmp_link = victoria_create_tmp_link.to_json()
                        victoria_create_tmp_link = json.loads(victoria_create_tmp_link)
                        cursor.execute("INSERT INTO public.users_tmp_invite_links (tg_user_id, login, chat_name, tmp_invite_link) VALUES ('" + str(message.from_user.id) + "', '" + str(user) + "', '" + str(victoria_tg_group_name) + "', '" + str(victoria_create_tmp_link["invite_link"]) + "')")
                        db.commit()
                        time.sleep(2)
                    except Exception as e:
                        print(e)
                user_victoria_telegram_groups = (user_victoria_telegram_groups + "Чат оповещения из VictoriaMetrics: " + str(victoria_tg_group_name) + "\nПригласительная ссылка: " + str(victoria_create_tmp_link["invite_link"]) + "\nAD группа: " + str(ad_group_body[0]))
            else:
                cursor.execute("SELECT org_name FROM public.vm_telegram_groups WHERE org_name = \'" + str(re.sub(r",", ".", str(i["orgName"]))) + "\'")
                created_org_name = cursor.fetchone()
                if created_org_name is None or created_org_name[0] == "ITMON":
                    continue
                cursor.execute("SELECT chat_id FROM public.vm_telegram_groups WHERE org_name = \'" + str(re.sub(r",", ".", str(i["orgName"]))) + "\'")
                created_chat_id = cursor.fetchone()
                cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE chat_name = \'" + str(victoria_tg_group_name) + "\' AND login = \'" + str(user) + "\'")
                victoria_create_tmp_link = cursor.fetchone()
                if victoria_create_tmp_link is None:
                    victoria_create_tmp_link = bot_poster.create_chat_invite_link(chat_id="-" + str(created_chat_id[0]), member_limit=1)
                    victoria_create_tmp_link = victoria_create_tmp_link.to_json()
                    victoria_create_tmp_link = json.loads(victoria_create_tmp_link)
                    cursor.execute("INSERT INTO public.users_tmp_invite_links (tg_user_id, login, chat_name, tmp_invite_link) VALUES ('" + str(message.from_user.id) + "', '" + str(user) + "', '" + str(victoria_tg_group_name) + "', '" + str(victoria_create_tmp_link["invite_link"]) + "')")
                    db.commit()
                    cursor.execute("SELECT tmp_invite_link FROM public.users_tmp_invite_links WHERE tg_user_id = \'" + str(message.from_user.id) + "\' AND login = \'" + str(user) + "\' AND chat_name = \'" + str(victoria_tg_group_name) + "\'")
                    victoria_create_tmp_link = cursor.fetchone()
                    time.sleep(5)
                if len(user_victoria_telegram_groups) < 2000:
                    user_victoria_telegram_groups = (user_victoria_telegram_groups + "Чат оповещения из VictoriaMetrics: " + str(created_org_name[0]) + "\nПригласительная ссылка: " + str(victoria_create_tmp_link[0])+"\nAD группа: " + str(ad_group_body[0]) + "\n---------------------------------------------------------------------------\n")
                else:
                    bot.send_message(message.chat.id, str(user_victoria_telegram_groups))
                    user_victoria_telegram_groups = ""
                    user_victoria_telegram_groups = (user_victoria_telegram_groups + "Чат оповещения из VictoriaMetrics: " + str(created_org_name[0]) + "\nПригласительная ссылка: " + str(victoria_create_tmp_link[0]) + "\nAD группа: " + str(ad_group_body[0]) + "\n---------------------------------------------------------------------------\n")

    if count_if_no_access_victoria == 0:
        bot.send_message(message.chat.id, "Группы для Вас не обнаружены.\nДля получения доступа Вам необходимо назначить группу графаны Вашей организации/АС")

    if len(user_victoria_telegram_groups) > 0:
        bot.send_message(message.chat.id, str(user_victoria_telegram_groups))
        user_victoria_telegram_groups = ""
    time.sleep(2)
    bot.send_message(message.chat.id, "Поиск групп VictoriaMetrics завершен.\n\nПочти готово!\n"
                                      "Для окончательной настройки необходимо убедиться в наличии записей routes и receivers в конфигурационном файле\n"
                                      "\"../configurations/alertmanager/alertmanager.yml\"\n"
                                      "При их отсутствии - добавить! Примеры есть в конфиге.\n\n"
                                      "В routes:\n\n"
                                      "    - matchers:\n"
                                      "      - project=\"краткое_имя_вашей_AC\"\n"
                                      "      receiver: '%ad_group%'\n"
                                      "В receivers:\n\n"
                                      "    - name: \"%ad_group%\"\n"
                                      "      webhook_configs:\n"
                                      "        - url: 'http://localhost:9119/alert'\n"
                                      "          send_resolved: true\n\n"
                                      "Вместо %ad_group% - нужно подставлять имя группы из сообщений выше если группы были найдены.")

    cursor.execute("SELECT cycle_breaker FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    cycle_breaker = cursor.fetchone()
    cycle_breaker = (int(cycle_breaker[0]) + 1)
    cursor.execute("UPDATE public.users SET cycle_breaker = \'" + str(cycle_breaker) + "\' WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    db.commit()
    cursor.execute("SELECT cycle_breaker FROM public.users WHERE tg_user_id = \'" + str(message.from_user.id) + "\'")
    cycle_breaker = cursor.fetchone()
    time.sleep(5)
    if int(cycle_breaker[0]) < 2:
        goto_zabbix(message)
    else:
        bot.send_message(message.chat.id, "Спасибо за использование бота мониторинга!")


def goto_zabbix(message):
    rmk = telebot.types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    rmk.add(telebot.types.KeyboardButton("Да"), telebot.types.KeyboardButton("Нет"))
    choice = bot.send_message(message.chat.id, "Хотите продолжить и поискать группы Zabbix?", reply_markup=rmk)
    bot.register_next_step_handler(choice, go_next_zabbix)


def go_next_zabbix(message):
    alert_system = message.text
    if alert_system == "Да":
        zabbix_show_alert_groups(message)
    elif alert_system == "Нет":
        bot.send_message(message.chat.id, "Ок, спасибо за использование бота мониторинга!")
    else:
        bot.send_message(message.chat.id, "Извините, но я Вас не понял. Попробуйте начать сначала!\n\n/start")


bot.polling(none_stop=True, interval=0)


